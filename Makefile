all: lint test

test:
	pytest-3 -v

lint:
	pycodestyle .
	pylint3 textrpg/*.py
