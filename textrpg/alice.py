from scenario import *


alice = Scenario("001", ['final'], {
    'is_small': False,
    }, [
    Scene("001", [
        SceneDescription("Vous êtes dans une pièce avec un couloir à gauche et une table au milieu."),
        SceneDescription("Une bouteille est posée sur la table.", "not is_small")],
        [
            Option("True", "Aller dans le couloir", ["moveto('002')"]),
            Option("not is_small", "Prendre la bouteille", ["moveto('003')"]),
        ]),
    Scene("002", [
        SceneDescription("Le couloir se termine sur une porte, fermée à clé. Il y a une toute petite ouverture en bas, juste de quoi passer un doigt.", "not is_small"),
        SceneDescription("Le couloir énorme se termine sur une porte géante. Il y a une ouverture à la bonne taille pour y passer.", "is_small")],
        [
            Option("is_small", "Passer par l'ouverture", ["moveto('final')"]),
            Option("True", "Retourner dans la pièce", ["moveto('001')"]),
        ]),
    Scene("003", [
        SceneDescription("La boutielle contient un liquide doré. Il est écrit 'boit-moi' sur une étiquette.")
        ],
        [
            Option("not is_small", "Boire la bouteille", ["moveto('004')"]),
            Option("True", "Reposer la bouteille sur la table", ["has_bottle=False", "moveto('001')"]),
        ]),
    Scene("004", [
        SceneDescription("Vous rétrecicez, jusqu'a devenir de la taille d'une petite souris !")
        ],
        [
            Option("not is_small", "Continuer", ["is_small=True, ""moveto('001')"]),
        ]),

    Scene("final", [
        SceneDescription("Vous êtes sorti \o/"),
    ], [])
])



g = Game(alice)
g.run(TextUI())
