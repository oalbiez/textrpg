# -*- coding: utf-8 -*-

class TextUI(object):
    def display_scene(self, description):
        print()
        print(description)
        print()

    def display_choice(self, index, description):
        print(index, ":", description)

    def input(self):
        return int(input("> "))


class Game(object):
    def __init__(self, scenario):
        self.__scenario = scenario
        self.__current_scene = scenario.start()
        self.__memory = {
            'moveto': self.moveto
        }
        self.__memory.update(scenario.state())

    def current_scene(self):
        return self.__scenario.scene(self.__current_scene)

    def moveto(self, reference):
        self.__current_scene = reference

    def set(self, key, value):
        self.__memory[key] = value

    def get(self, key):
        return self.__memory[key]

    def evaluate(self, expression):
        return eval(expression, self.__memory)

    def apply(self, expression):
        return exec(expression, self.__memory)

    def run(self, ui):
        while True:
            scene = self.current_scene()
            ui.display_scene(scene.description(self))
            if self.__scenario.is_final(scene.reference()):
                return
            choices = [option for option in scene.options() if option.is_active(self)]
            for index,choice in enumerate(choices):
                ui.display_choice(index+1, choice.description())
            user_input = ui.input()
            choices[user_input-1].play(self)


class Scenario(object):
    def __init__(self, start, finals, state, scenes):
        self.__start = start
        self.__finals = finals
        self.__state = state
        self.__scenes = dict((s.reference(), s) for s in scenes)

    def start(self):
        return self.__start

    def is_final(self, state):
        return state in self.__finals

    def state(self):
        return self.__state

    def scene(self, reference):
        return self.__scenes[reference]


class Scene(object):
    def __init__(self, reference, descriptions, options):
        self.__reference = reference
        self.__descriptions = descriptions
        self.__options = options

    def reference(self):
        return self.__reference

    def description(self, game):
        return " ".join(part.description(game) for part in self.__descriptions)

    def options(self):
        return self.__options


class SceneDescription(object):
    def __init__(self, description, predicate="True"):
        self.__description = description
        self.__predicate = predicate

    def description(self, game):
        if game.evaluate(self.__predicate):
            return self.__description
        return ""


class Option(object):
    def __init__(self, predicate, description, actions):
        self.__predicate = predicate
        self.__description = description
        self.__actions = actions

    def description(self):
        return self.__description

    def is_active(self, game):
        return game.evaluate(self.__predicate)

    def play(self, game):
        for action in self.__actions:
            print(action)
            game.apply(action)
